package com.example.leave.service;

import com.example.leave.model.BucketApproval;
import com.example.leave.repository.BucketApprovalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BucketApprovalServices {

    @Autowired
    BucketApprovalRepository bucketApprovalRepository;

    public void inputData(BucketApproval bucketApproval) {
        bucketApprovalRepository.save(bucketApproval);
    }

    public List<BucketApproval> getDataAll() {
        return bucketApprovalRepository.findAll();
    }

    public BucketApproval getDataById(Long userId) {
        Optional<BucketApproval> opt = bucketApprovalRepository.findById(userId);
        if (opt.isPresent()) {
            return opt.get();
        }else {
            return null;
        }
    }

    public void deleteData(BucketApproval bucketApproval) {
        bucketApprovalRepository.delete(bucketApproval);
    }
}
