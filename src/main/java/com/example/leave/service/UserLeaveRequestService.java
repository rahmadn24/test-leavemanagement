package com.example.leave.service;

import com.example.leave.model.UserLeaveRequest;
import com.example.leave.repository.UserLeaveRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserLeaveRequestService {
    @Autowired
    UserLeaveRequestRepository userLeaveRequestRepository;

    public void inputData(UserLeaveRequest userLeaveRequest) {
        userLeaveRequestRepository.save(userLeaveRequest);
    }

    public List<UserLeaveRequest> getDataAll() {
        return userLeaveRequestRepository.findAll();
    }

    public List<UserLeaveRequest> getDataAllByUserId(Long user, Pageable pageable) {
        System.out.println(user);
        System.out.println(pageable);
        return userLeaveRequestRepository.findByUser(user, pageable);
    }

    public UserLeaveRequest getDataById(Long userId) {
        Optional<UserLeaveRequest> opt = userLeaveRequestRepository.findById(userId);
        if (opt.isPresent()) {
            return opt.get();
        }else {
            return null;
        }
    }

    public void deleteData(UserLeaveRequest userLeaveRequest) {
        userLeaveRequestRepository.delete(userLeaveRequest);
    }
}
