package com.example.leave.service;

import com.example.leave.model.Position;
import com.example.leave.model.PositionLeave;
import com.example.leave.repository.PositionLeaveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PositionLeaveServices {

    @Autowired
    PositionLeaveRepository positionLeaveRepository;

    public void inputData(PositionLeave positionLeave) {
        positionLeaveRepository.save(positionLeave);
    }

    public List<PositionLeave> getDataAll() {
        return positionLeaveRepository.findAll();
    }

    public List<PositionLeave> getDataAllById(Long positionLeaveId) {
        return positionLeaveRepository.findByPositionLeaveId(positionLeaveId);
    }

    public  PositionLeave getDataByPosition(Position position) {
        Optional<PositionLeave> opt = positionLeaveRepository.findByPosition(position);
        if (opt.isPresent()) {
            return opt.get();
        }else {
            return null;
        }
    }

    public PositionLeave getDataById(Long positionLeaveId) {
        Optional<PositionLeave> opt = positionLeaveRepository.findById(positionLeaveId);
        if (opt.isPresent()) {
            return opt.get();
        }else {
            return null;
        }
    }

    public void deleteData(PositionLeave positionLeave) {
        positionLeaveRepository.delete(positionLeave);
    }
}
