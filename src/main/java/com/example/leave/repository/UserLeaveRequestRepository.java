package com.example.leave.repository;

import com.example.leave.model.UserLeaveRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserLeaveRequestRepository extends JpaRepository<UserLeaveRequest, Long> {
    List<UserLeaveRequest> findByUser(Long user, Pageable pageable);
}
