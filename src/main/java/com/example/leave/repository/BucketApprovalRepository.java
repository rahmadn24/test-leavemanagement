package com.example.leave.repository;

import com.example.leave.model.BucketApproval;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Long> {
}
