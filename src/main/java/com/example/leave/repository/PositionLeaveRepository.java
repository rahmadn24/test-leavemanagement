package com.example.leave.repository;

import com.example.leave.model.Position;
import com.example.leave.model.PositionLeave;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PositionLeaveRepository extends JpaRepository<PositionLeave, Long> {

    List<PositionLeave> findByPositionLeaveId(Long positionLeaveId);
    Optional<PositionLeave> findByPosition(Position position);
}
