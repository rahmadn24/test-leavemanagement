package com.example.leave.utils;

public class Constant {
    public static final String CONST_DATA	= "data";
    public static final String CONST_STATUS	= "status";
    public static final String CONST_MSG	= "message";
    public static final String CONST_ERROR	= "error";
}
